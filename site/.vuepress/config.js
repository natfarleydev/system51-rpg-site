module.exports = {
  title: "System51 RPG",
  description: "Information on the System51 campaign setting.",
  base: "/system51-rpg-site/",
  dest: "public",
  themeConfig: {
    sidebar: ["/", "sessions/", "npcs/", "locations/", "/gameaids.md"],
    nav: [{ text: "Home", link: "/" }]
  }
};
