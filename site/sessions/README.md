# Session summaries

Summaries of our sessions

## 1x07 Craig's Promise

As the team wait outside for the office for their next mission, they hear [Debbie](/npcs/#debbie-dyers) arguing with her sister ([Leslie “Trunchball” Dyers](/npcs/#leslie-truchball-dyers)) about pay. 

Debbie leaves, and the team get a debrief on [Kyoko](/npcs/#kyoko-don-yokuna)'s journal

* Clear meglomaniac
* Believes she has a right to the throne of Xyron
* Intent on killing the sole heir to the throne

Afterwards, [Craig](/npcs/#craig-zanderthip) turns up and briefs them on a job. Simply transport a refrigerator to a lab on the [asteroid belt](/locations/#asteroid-belt). And he *promises* it will be worth it.

Unfortunatley, due to station remodelling, Trunchball kicks the team out of their usual quarters and gives them a ship (which they must maintain with their own wages). Since none of them are trained in piloting a spacecraft, they hire a deaf man simply named [Derek](/npcs/#derek-dinath).

<iframe src="https://giphy.com/embed/3o7TKnCQ0oSfK8x1Ly" width="480" height="270" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/brooklynninenine-fox-brooklyn-nine-nine-brooklyn99-3o7TKnCQ0oSfK8x1Ly">via GIPHY</a></p>

Deaf and proud of it, Derek quickly establishes that he cards about flying the ship, and not about making friends.

On the way to the lab, the newest team member confronts Thomas about who he is. After an intense standoff, he leaves with neither person discovering much about the other. Naturally, the hacker of the group listened in. Thomas refused to give him any information to, simply raising his drink to the camera and walking off when asked over the intercom.

Meanwhile, Grumps can't help himself, and finds the cargo with a very complex lock ... that hasn't been locked! Opening the fridge, he sees a woman, grey, and almost lifeless.

Before he can evaluate much more, the ship is gripped by a pirate claw! Ripping a hole in the cargo bay and evacuating it and after a brief but intense fight the pirates attempt to steal the woman in stasis. Against all odds, the team manage to recover the woman, and the pirates leave with a hole in their hull.

Will they still make it safely? will others come back for the woman? Can she be revived? All this and more ... next time!

## 1x06 Health and Safety Issues
 Hot on the tails of the dead scientist, [Debbie](/npcs/#debbie-dyers) runs in to deal with the issue and do damage control. As she blackens the viewing window, suddenly the ground shakes as shots start and stop again moments later. Debbie walks out, neatens her dress and leaves.

As the team investigate the rooms (and lift the teleportation boxes) they find the room that used to have a severed hand in has a large, deep hole in it. Before more investigation can take place, the guards come and lock down the room.

Suspicious of this newcomer, the team have Jay do some research and find her ['HoneyShields' profile](https://nasfarley88.gitlab.io/debbies-honeyshield-profile)

As Grumps heads to the 2nd floor spa and bar, the rest of the team join him and find Debbie drinking away her sorrows. Turns out, her main business as a bounty hunter isn't going so well so she moonlights as a 'premium economy' bodyguard with HoneyShields but her last two clients turned out to be criminals so she had to turn them in to the authorities and the scientist she was just protecting disappeared in his teleportation portal (see above), so she has enough for one last drink.

Seeing an opportunity, Nasty drifts off and steals items from the patrons and secures some jewels and a phone from a guy named ‘[Craig](/npcs/#craig-zanderthip)’. He later hides the jewels on Debbie (as the fall girl) and Craig’s phone in a nearby vase.

Once Debbie had become more pliable (after several drinks provided by the teams expenses), Thomas interrogates her gently to find out who she is and what she saw in that room. Bidden to secrecy by her employer, she is initially unwilling but reveals that there was some kind of pangolin monster! Too drunk to properly describe it, all that can be gathered is that it made a large hole in the ground and disappeared and is likely still on the grounds ... somewhere.

She also reveals that she HATES her sister, [Leslie “Trunchball” Dyers](/npcs/#leslie-truchball-dyers)! The teams very own boss!

The morning after, [Kyoko](/npcs/#kyoko-don-yokuna) (the embassy lead) takes everyone aside and takes them into her office and offers them payment for the box and death of the scientist (after some haggling about whether the team actually killed the scientist by Nasty). The group meet Craig, someone who insists they know Thomas as ‘John Kravitz’ and won’t leave till the group promise to buy some refrigerators. After that, Kyoko leaves the teamin her office to go and get the ‘paperwork’ as the team search the room for anything valuable. After some time, they realise they’ve been betrayed and poison gas is filling the room! Escaping through a secret passage, they manage to take a journal and some secret documents.

On their way out, the team hatch a plan to escape via the caterers entrance. But just as they find the uniforms, the ground shakes and the kaiju CATOLIN appears! Emerging from the ground, the long and large mass of scales was only interrupted by legs coming out at all angles. Ribbons eked out from the scales appearing to defy gravity. The terrified guests shrieked as the catolin wraps its many legs around a nearby guest, opens it’s scales and slices into the them, carving them from every angle as it consumes them, inch by terrifying inch. No sooner than it’s done, the catolin disappears into the ground.

Acting on instinct, Grumps throws a grenade, which the catolin runs towards and becomes damaged. Seeing their chance, the group run out of the front door, seeing a hungover Debbie the other side of the catolin.

Will Debbie get out alive? What is contained in those secret documents? What will the fallout be from *yet another* job without payment?

All this and more, next session!

## 1x05 The Fox with No Neck
* Win the Fennec Fox edition of Crufts ☑️
* Not get caught poisoning all the other foxes at the Fennec Fox edition of Crufts ☑️
* Meet a woman named Debbie ☑️
* Persuade a slave that he should serve the PCs instead of his current master ☑️
* Convince said master (via the slave) to jump through an untested portal leaving only a severed hand ☑️

## 1x04 The Best Laid Plans

James Baron wakes up on the ship, still healing from his sniper injury. He suddenly becomes aware of some madman babbling about healing his injury. Before knows it, he's injected with a 2ft syringe and passes out

Erin starts to become lucid and causes trouble for Jay and Grumps so they lock her in one of the rooms of the ship and disconnect her room. Blip (Grump’s droid) goes into the building and incapacitates a guardsman

James wakes up again, this time with several doctors and nurses around him. His leg is healed! But there's something else ... he feels around his face and feels a full foot of beard! "How long have I been out?" he asks. "About 2 and a half hours?!" the doctor exclaims.

And then the gravity stops on the station. Things resembling shirukens slice through the air, killing many of the doctors. A mysterious stranger thuds through the door with magnetic boots. A stranger to everyone but James.

"Hello Dimitri" James says wryly. They continue to talk about how James has left his post, that 'Father' wants him to come back. James refuses. Dimitri throws his sword. James fires his guns.

At the same moment, communications blackout on the surface and the building with Johanna in explodes. Erin figures out how to leave the ship, while Jay desperately tries to fend of the virus that has affected the ship and the station. Grumps wisely isolates the weapons systems from the ship, leaving them defenseless, but keeping the virus from using them instead. Grumps commands Blip (his droid) to go and find the girl.

As Blip goes around, it meets a beautiful woman, Erin! It immediately fires, thinking Erin a threat, but Erin goes into a trance and quickly puts in a command code and Blip identifies her as a technician. How does Erin have this knowledge? By rights she shouldn’t, but the team don’t have time to figure that out now.

Dimitri and James continue fighting until James blows a hole in Dimitri’s leg. They discuss more about returning home, and about why someone has been sent now, after all this time. James then goes off to find someone to fix the station and finds a communications engineer. During this time, Dimitri escapes leaving only a bloody stump.

Jay finally gets into contact with the ship, gettings whispers of something called ‘Albion’, a name associated with a group that left System50 several generations ago. He also ascertains that the virus is of unknown origin (i.e. alien, at least to him) and it is attempting to use all ship and station resources to communicate outwards.

Meanwhile, Erin and Blip find little Johanna, terrify her (because neither a robot nor a crazy person are great with kids), and bring her back to the ship (after Blip critical successes and literally takes out the big bad’s Achilles tendon, blood everywhere, etc.)

With help from Jay on the surface, James finds a satellite off the stations bow which is responsible for the transmissions. Sleek and utilitarian, he recognises the technology but with no further markings he decides to snap off the antennae and bring it in. As he does, he sees Trunchball standing in the airlock looking furious...

## 1x03 Is this the real life? Is this [a deadly neurotoxin]?
* The weird 'Chaos Machine' portal thing closed just as something sinister seemed to appear on the other side
* Security came up the lift, Grumps exploded the lift, security went down the lift ... in pieces.
* Trunchball called to see if the team was in the area to assist at an incursion at INGEN
* Thomas bluffed that it would all be fine and you would be happy to help
* Jay and Thomas hacked in, wiped the computer system
* Protector James Baron got shot in the leg by a crazy sniper
* Grumps started shooting a laser gattling gun at said sniper
* Jay and Thomas said hi in the ship, and then blew him to pieces
* Nasty McShandy executed any ... intelligible witnesses
* He also stole the chip and blueprints for the Chaos Machine.
* The team returned to Finn and Xi and Nasty handed over the chip in return for the location of the kidnapped girl
* Xi and Nasty cased the joint and found 12 armed people going in and out
* Disarmed one entrance by accident
* Thomas had the oil identified, it belongs to an older model robotic arm that's leaking badly, meaning it's almost definitely a huge guy with a robot arm who's taken 3yo Johanna
* An assault is planned, 3 pronged attack
* Xi and Nasty go in the sneaky way and successfully wipe out the 2 sent to investigate the incursion in 2 seconds

... leaving the rest of the teams (team Distraction and team ??) to enter next time!

## 1x01 Pilot
<npc-image src="http://www1.pictures.zimbio.com/mp/FqiXeFgLtipl.jpg" credit-link="http://www1.pictures.zimbio.com/mp/FqiXeFgLtipl.jpg" credit-author="Google images"></npc-image>

* The team is a sort of guns for hire/no nationality kind of group
* They go back to base (on a space station) and their boss, Trunchball (pictured) tells them they've killed the wrong guy on the last mission!
* Next mission they have (now that they are on thin ice) is a kidnapping of a 3yo daughter, Joanne, from a CEO, Nicholas Johns, at INGEN, a cybernetics company.
* Nicholas is clearly cagey about the demands but hires the team to recover his daughter.
* The team shadow N. and his wife (Mrs. Johns), back to a LARGE company building that requires pass-card and fingerprint access
* Xi follows an employee on a cigarette break into an alley (the team follow)
* Said employee is puffing bouts of rainbow coloured smoke, which forms different pictures based on emotions
* Xi Fast Draw’s his sword (even as Thomas, tries to stop him) and swipes employee’s head straight off! His head tumbles to the ground, cigarette still in mouth, with a 😱 formed by the smoke as the head settles
* Shenanigans ensue trying to use the hand to enter a back door (they got the wrong hand) then security comes round the corner and the team leg it over the fence
* The team then go and see Finn, a sort of intermediary for independent contractors (i.e. people that do what the team does without company backing) who offers them information in return for stealing something from INGEN
* The team accept, apart from Thomas who wants nothing to do with thieving a corporation, so he goes to talk to the maid at Nicholas Johns house
* Thomas discovers the maid has evidence of who was in the house but hid it from the police for fear her employer would sell her over the messy room
* Thomas also discovers an oily substance outside (very unusual for this future world) which he has a sample of for analysis
* The rest of the team get into the ground floor of the INGEN building as janitors.
* They convince the head janitor (who really doesn’t care about his job) to give them access to several of the upper floors